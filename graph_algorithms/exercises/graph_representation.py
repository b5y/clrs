from graph_algorithms.graph_abstract_data_type import Graph

# Exercise 22.1-3.

def transpose_graph_adj_list(g):
    """
    Transposing graph g from adjacency-list representation.
    @:param g: input graph
    @:param gt: output graph
    Time: O (E + V)
    """
    gT = Graph()
    for v in g:
        for w in v.get_connections():
            if gT.get_vertex(v) is None:
                gT.add_edge(v.get_id(), w.get_id(), w.get_weight())
    return gT


"""
TODO: Transposing graph from matrix representation
def transpose_graph_adj_matrix(g, gT):
    for v in g:
        for w in v.get_connections():
            gT[j,i] = g[i,j]
            j = j + 1
        i = i + 1
Time: O (V^2)
"""

# Exercise 22.1-5.
"""
TODO:
def square_graph_adj_list(g):

    #Squaring graph from adjacency list representation.
    #@:param g: input graph
    #@:param gS: output graph

    return True
By the way, needs (or should) to implement Strassen algorithm or Coppersmith–Winograd algorithm
"""
