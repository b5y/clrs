""" The idea of graph ADT implementation was borrowed from
http://interactivepython.org/runestone/static/pythonds/index.html
with minor changes.
"""
class Vertex:
    """Representing each vertex in the graph as adjacency list"""

    def __init__(self, node):
        self.id = node
        self.adjacent = {}

    def add_neighbour(self, nbr, weight=0):
        """Adding connection from current vertex to another"""
        self.adjacent[nbr] = weight

    def __str__(self):
        """print out connected vertices
        another idea (on Graph class):
        def __str__(self):
            return '{}({})'.format(self._class_._name_, dict(self._graph))
        """
        return str(self.id) + ' adjacent: ' + \
               str([x.id for x in self.adjacent])

    def get_connections(self):
        """Returning all vertices in the adjacency list"""
        return self.adjacent.keys()

    def get_id(self):
        return self.id

    def get_weight(self, nbr):
        return self.adjacent[nbr]

class Graph:
    """creates new, empty graph
    another idea with collections from defaultdict:
    def __init__(self, connections, directed=False):
        self.vert_list = defaultdict(set)
        self._directed = directed
        self.add_connections(connections)
    """
    def __init__(self):
        self.vert_list = {}
        self.num_vertices = 0

    def add_vertex(self, key):
        """Adds an instance of Vertex to the graph"""
        self.num_vertices = self.num_vertices + 1
        new_vertex = Vertex(key)
        self.vert_list[key] = new_vertex
        return new_vertex

    def get_vertex(self, n):
        """Finds the vertex in the graph"""
        if n in self.vert_list:
            return self.vert_list[n]
        else:
            return None

    def __contains__(self, n):
        return n in self.vert_list

    def add_edge(self, frm, to, cost=0):
        """Adds a new, directed edge to the graph that connects two vertices.
        @param frm: from first vertex
        @param to: to second one
        """
        if frm not in self.vert_list:
            nv = self.add_vertex(frm)
        if to not in self.vert_list:
            nv = self.add_vertex(to)
        self.vert_list[frm].add_neighbour(self.vert_list[to], cost)

    def remove(self, node):
        """Remove all references to node
         @:param node: input node
        """
        for n, m in self.vert_list.__iter__():
            try:
                m.remove(node)
            except KeyError:
                pass
        try:
            del self.vert_list[node]
        except KeyError:
            pass

    def get_vertices(self):
        """Returns the list of all vertices in the graph"""
        return self.vert_list.keys()

    def __iter__(self):
        """Iterating over all vertex objects in graph"""
        return iter(self.vert_list.values())

    def find_isolated_vertices(self):
        """Returns a list of isolated vertices."""
        graph = self.vert_list
        isolated = []
        for v in graph:
            print(isolated, v)
            if not graph[v]:
                isolated += [v]
        return isolated

    def find_path(self, start_v, end_v, path=[]):
        """Find a path from start_v to end_v in graph
        @:param start_v: start from vertex
        @:param end_v: final on vertex
        """
        path = path + [start_v]
        if start_v == end_v:
            return path
        if start_v not in self.vert_list:
            return None
        for v in self.vert_list[start_v]:
            if v not in path:
                extended_path = self.find_path(v, end_v, path)
                if extended_path:
                    return extended_path
        return None

    def find_all_paths(self, start_v, end_v, path=[]):
        """Find all paths from start_v to end_v in graph
        @:param start_v: start vertex
        @:param end_v: final vertex
        """
        graph = self.vert_list
        path = path + [start_v]
        if start_v == end_v:
            return [path]
        if start_v not in graph:
            return []
        paths = []
        for v in graph[start_v]:
            if v not in path:
                extended_path = self.find_all_paths(v, end_v, path)
                for p in extended_path:
                    paths.append(p)
        return paths

    def is_connected(self, first_v, second_v):
        """Is first_v directly connected to second_v?
        @:param first_v: first vertex
        @param second_v: second vetex
        """
        return first_v in self.vert_list and second_v in self.vert_list
